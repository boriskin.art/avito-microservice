# О проекте
#### Реализовал все что требовалось в ТЗ - [Ссылка на ТЗ](https://github.com/avito-tech/autumn-2021-intern-assignment)

### Дополнительно:

1. Человеко-читаемые ошибки с кодами


2. Методы списания и начисления средств объединил в один


3. Написаны Unit тесты для репозитория


4. Использован **docker** / **docker compose** для поднятия и развертывания dev-среды. 


### Дополнительные задания
1. Реализован метод получения средств пользователя в отличной от рубля валюте. 

Использовал [https://www.cbr-xml-daily.ru/](https://www.cbr-xml-daily.ru/)


# Что под капотом?
1. Роутер - `Gorilla Mux`


2. База данных - `PostgreSQL` (docker-compose), драйвер `pgx` потому что модно, а еще быстрее `lib/pq` на 63%


3. Configs - `Viper` и `yaml файл`


4. Тестирование - `mockery` - [ссылка](https://github.com/vektra/mockery)


5. Валидация входных json данных - [govalidator](https://github.com/asaskevich/govalidator)

# С какими вопросами столкнулся?

1. Что делать, если пришел запрос на проверку баланса для пользователя, которого еще нет в базе данных.
   
Решил возвращать ошибку, а не нулевой баланс.

2. Что делать если перевод средств идет еще не созданному пользователю?

Решил создавать его и начислять средства, а не отменять транзакцию.


# Как запустить?
### 1. Docker-compose 
Шаблон файла лежит в корне проекта

При необходимости меняем переменные окружения и выполняем команду в терминале

`docker-compose up -d`

В поднятом postgres контейнере выполняем 

`psql -U <username> -d wallet`

`username` - по-умолчанию `user`

Выполняем код из файла `DATABASE.sql` для создания базы данных.

### 2. Локально

В папке configs, в файле config.yml меняем настройки подключения к базе данных

Создаем базу данных (код для создания таблиц в файле `DATABASE.sql`)

Выполняем команду в терминале

`go run ./cmd/wallet_service/*`

### 3. Использовать запущенную версию проекта

[http://89.111.136.40/](http://89.111.136.40/)

# Список возможных ошибок
```
100 - User doesn't exist

200 - Not enough money
201 - Incorrect amount

300 - Invalid operation type
301 - Error while parsing url parameters
302 - Input data error

400 - Error while transferring funds

500 - Server error

600 - Get currency API error
601 - Parse currency data error

700 - Input data error
```

# Примеры запросов
## Для локального проекта

1. Получение баланса ID = 1 пользователя в рублях

```http request
curl --location --request GET 'http://localhost:8080/wallet/1'
```


```json
{
  "status_code": 0,
  "response": {
    "user_balance": 200
  }
}
```

2. Получение баланса пользователя ID = 1 в иной валюте

```http request 
curl --location --request GET 'http://localhost:8080/wallet/1?currency=USD'
```

```json
{
  "status_code": 0,
  "response": {
    "user_balance": 2.73
  }
}
```

3. Получение баланса несуществующего пользователя

```http request 
curl --location --request GET 'http://localhost:8080/wallet/555'
```

```json
{
  "status_code": 100,
  "error_message": "User doesn't exist"
}
```

4. Начисление средств пользователю ID = 1

Operation type "refill" - начисление средств

```http request 
curl --location --request POST 'http://localhost:8080/wallet/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "user_id": 1,
    "operation_type": "refill",
    "amount": 20
}'
```

```json
{
  "status_code": 0,
  "response": {
    "message": "ok"
  }
}
```

5. Списание средств с пользователя ID = 1

Operation type "expenditure" - списание средств

```http request 
curl --location --request POST 'http://localhost:8080/wallet/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "user_id": 1,
    "operation_type": "expenditure",
    "amount": 10
}'
```

```json
{
  "status_code": 0,
  "response": {
    "message": "ok"
  }
}
```

6. Списание средств с пользователя ID = 1, при недостаточном балансе

```http request 
curl --location --request POST 'http://localhost:8080/wallet/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "user_id": 1,
    "operation_type": "expenditure",
    "amount": 1000
}'
```

```json
{
  "status_code": 200,
  "error_message": "Not enough money"
}
```

7. Перевод средств от ID = 1 к ID = 2

```http request 
curl --location --request POST 'http://localhost:8080/wallet/transfer' \
--header 'Content-Type: application/json' \
--data-raw '{
    "from_user_id": 1,
    "to_user_id": 2,
    "amount": 10
}'
```

```json
{
  "status_code": 0,
  "response": {
    "message": "ok"
  }
}
```

8. Перевод средств от ID = 1 к ID = 2 при недостаточном балансе ID = 1

```http request 
curl --location --request POST 'http://localhost:8080/wallet/transfer' \
--header 'Content-Type: application/json' \
--data-raw '{
    "from_user_id": 1,
    "to_user_id": 2,
    "amount": 1000
}'
```

```json
{
  "status_code": 200,
  "error_message": "Not enough money"
}
```

9. Перевод средств от ID = 1 к ID = 2 если ID = 1 нет в базе данных

```http request 
curl --location --request POST 'http://localhost:8080/wallet/transfer' \
--header 'Content-Type: application/json' \
--data-raw '{
    "from_user_id": 55,
    "to_user_id": 2,
    "amount": 1000
}'
```

```json
{
  "status_code": 100,
  "error_message": "User doesn't exist"
}
```

## Для проекта на моем хосте
1. Получение баланса ID = 1 пользователя в рублях

```http request
curl --location --request GET 'http://89.111.136.40/wallet/1'
```


```json
{
  "status_code": 0,
  "response": {
    "user_balance": 200
  }
}
```

2. Получение баланса пользователя ID = 1 в иной валюте

```http request 
curl --location --request GET 'http://89.111.136.40/wallet/1?currency=USD'
```

```json
{
  "status_code": 0,
  "response": {
    "user_balance": 2.73
  }
}
```

3. Получение баланса несуществующего пользователя

```http request 
curl --location --request GET 'http://89.111.136.40/wallet/555'
```

```json
{
  "status_code": 100,
  "error_message": "User doesn't exist"
}
```

4. Начисление средств пользователю ID = 1

Operation type "refill" - начисление средств

```http request 
curl --location --request POST 'http://89.111.136.40/wallet/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "user_id": 1,
    "operation_type": "refill",
    "amount": 20
}'
```

```json
{
  "status_code": 0,
  "response": {
    "message": "ok"
  }
}
```

5. Списание средств с пользователя ID = 1

Operation type "expenditure" - списание средств

```http request 
curl --location --request POST 'http://89.111.136.40/wallet/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "user_id": 1,
    "operation_type": "expenditure",
    "amount": 10
}'
```

```json
{
  "status_code": 0,
  "response": {
    "message": "ok"
  }
}
```

6. Списание средств с пользователя ID = 1, при недостаточном балансе

```http request 
curl --location --request POST 'http://89.111.136.40/wallet/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "user_id": 1,
    "operation_type": "expenditure",
    "amount": 1000
}'
```

```json
{
  "status_code": 200,
  "error_message": "Not enough money"
}
```

7. Перевод средств от ID = 1 к ID = 2

```http request 
curl --location --request POST 'http://89.111.136.40/wallet/transfer' \
--header 'Content-Type: application/json' \
--data-raw '{
    "from_user_id": 1,
    "to_user_id": 2,
    "amount": 10
}'
```

```json
{
  "status_code": 0,
  "response": {
    "message": "ok"
  }
}
```

8. Перевод средств от ID = 1 к ID = 2 при недостаточном балансе ID = 1

```http request 
curl --location --request POST 'http://89.111.136.40/wallet/transfer' \
--header 'Content-Type: application/json' \
--data-raw '{
    "from_user_id": 1,
    "to_user_id": 2,
    "amount": 1000
}'
```

```json
{
  "status_code": 200,
  "error_message": "Not enough money"
}
```

9. Перевод средств от ID = 1 к ID = 2 если ID = 1 нет в базе данных

```http request 
curl --location --request POST 'http://89.111.136.40/wallet/transfer' \
--header 'Content-Type: application/json' \
--data-raw '{
    "from_user_id": 55,
    "to_user_id": 2,
    "amount": 1000
}'
```

```json
{
  "status_code": 100,
  "error_message": "User doesn't exist"
}
```


