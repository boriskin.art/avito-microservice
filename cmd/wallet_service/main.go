package main

import walletService "avito-microservice/internal/wallet-service"

func main() {
	walletService.Run()
}
