CREATE TABLE wallet (
  user_id int primary key,
  balance real not null
);