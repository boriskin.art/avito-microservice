# Build binary with downloaded go modules
FROM golang:1.16.7-buster as build

# Copy files
RUN mkdir /home/wallet_service
COPY ./go.mod /home/wallet_service
WORKDIR /home/wallet_service

# Download go modules to cache
RUN go mod download
COPY . /home/wallet_service

# Build application
RUN CGO_ENABLED=0 go build -o wallet_service ./cmd/wallet_service/*

# Start app in low-size alpine image
FROM alpine:3.14.1

RUN mkdir wallet_service

# Copy files from build step
WORKDIR wallet_service
COPY --from=build /home/wallet_service/wallet_service .
RUN mkdir configs
COPY --from=build /home/wallet_service/config ./config

# Start app
CMD ["./wallet_service"]

