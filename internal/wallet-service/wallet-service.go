package wallet_service

import (
	"avito-microservice/config"
	"avito-microservice/internal/delivery/api"
	"avito-microservice/internal/repository/psql"
	"avito-microservice/internal/usecase"
	"avito-microservice/pkg/db_connections"
	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net/http"
	"time"
)

func init() {
	govalidator.SetFieldsRequiredByDefault(true)
}

func Run() {
	config.InitConfig()

	router := mux.NewRouter()

	walletRepository := psql.NewWalletPsqlRepository(db_connections.GetPostgres())
	walletUsecase := usecase.NewWalletUsecase(walletRepository)
	api.NewWalletHandler(router, walletUsecase)


	srv := &http.Server{
		Addr:         viper.GetString("port"),
		Handler:      router,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	logrus.Info("Starting server...")

	if err := srv.ListenAndServe(); err != nil {
		logrus.Fatal("Server error:", err)
	}
}
