package api

import (
	"avito-microservice/internal/delivery"
	"avito-microservice/internal/domain"
	"database/sql"
	"encoding/json"
	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

type WalletHandler struct {
	WalletUsecase domain.WalletUsecase
}

func NewWalletHandler(router *mux.Router, wu domain.WalletUsecase) {
	handler := WalletHandler{WalletUsecase: wu}

	router.HandleFunc("/wallet/{id:[0-9]+}", handler.GetBalance).Methods("GET")
	router.HandleFunc("/wallet/", handler.UpdateBalance).Methods("POST")
	router.HandleFunc("/wallet/transfer", handler.TransferFunds).Methods("POST")
}

func (wh *WalletHandler) GetBalance(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		delivery.NewErrorResponse(w, domain.URLParamsError)
		return
	}

	userBalance, err := wh.WalletUsecase.GetBalance(userID)
	if err == sql.ErrNoRows {
		delivery.NewErrorResponse(w, domain.UserDoesNotExistError)
		return
	}
	if err != nil {
		delivery.NewErrorResponse(w, domain.NewWalletError(500, "Get balance error"))
		return
	}

	currency := r.FormValue("currency")
	userBalance, e := wh.WalletUsecase.ConvertCurrency(currency, userBalance)
	if e != nil {
		delivery.NewErrorResponse(w, e)
		return
	}

	delivery.NewSuccessResponse(w, delivery.UserBalanceResponse{
		UserBalance: userBalance,
	})
}

func (wh *WalletHandler) UpdateBalance(w http.ResponseWriter, r *http.Request) {
	updateBalanceInput := delivery.UpdateBalanceInput{}

	err := json.NewDecoder(r.Body).
		Decode(&updateBalanceInput)
	if err != nil {
		delivery.NewErrorResponse(w, domain.InputDataError)
		return
	}

	_, err = govalidator.ValidateStruct(updateBalanceInput)
	if err != nil {
		delivery.NewErrorResponse(w, domain.NewWalletError(700, "Input data error: " + err.Error()))
		return
	}

	e := wh.WalletUsecase.UpdateBalance(
		updateBalanceInput.UserID,
		updateBalanceInput.OperationType,
		updateBalanceInput.Amount)


	if e != nil {
		delivery.NewErrorResponse(w, e)
		return
	}

	delivery.NewSuccessResponse(w, delivery.UpdateBalanceResponse{Message: "ok"})
}

func (wh *WalletHandler) TransferFunds(w http.ResponseWriter, r *http.Request) {
	transferFundsInput := delivery.TransferFundsInput{}

	err := json.NewDecoder(r.Body).
		Decode(&transferFundsInput)
	if err != nil {
		delivery.NewErrorResponse(w, domain.URLParamsError)
		return
	}

	_, err = govalidator.ValidateStruct(transferFundsInput)
	if err != nil {
		delivery.NewErrorResponse(w, domain.NewWalletError(700, "Input data error: " + err.Error()))
		return
	}

	if err := wh.WalletUsecase.TransferFunds(
		transferFundsInput.FromUserID,
		transferFundsInput.ToUserID,
		transferFundsInput.Amount,
	); err != nil {
		delivery.NewErrorResponse(w, err)
		return
	}

	delivery.NewSuccessResponse(w, delivery.TransferFundsResponse{Message: "ok"})
}