package delivery

import (
	"avito-microservice/internal/domain"
	"encoding/json"
	"net/http"
)

type UpdateBalanceInput struct {
	UserID 	        int		  `json:"user_id" valid:"required,int"`
	OperationType   string    `json:"operation_type" valid:"required,utfletter"`
	Amount          float64   `json:"amount" valid:"required,float"`
}

type TransferFundsInput struct {
	FromUserID 	 int	  `json:"from_user_id" valid:"required,int"`
	ToUserID 	 int      `json:"to_user_id" valid:"required,int"`
	Amount 		 float64  `json:"amount" valid:"required,float"`
}

type SuccessResponse struct {
	StatusCode 	int 		  `json:"status_code"`
	Response 	interface{}	  `json:"response"`
}

type UserBalanceResponse struct {
	UserBalance float64   `json:"user_balance"`
}

type UpdateBalanceResponse struct {
	Message  string  `json:"message"`
}

type TransferFundsResponse struct {
	Message  string `json:"message"`
}

type ErrorResponse struct {
	StatusCode 	 int	  `json:"status_code"`
	ErrorMessage string   `json:"error_message"`
}

func NewSuccessResponse(w http.ResponseWriter, response interface{}) {
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(SuccessResponse{
		StatusCode:  0,
		Response: response,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func NewErrorResponse(w http.ResponseWriter, walletError *domain.WalletError) {
	w.Header().Set("Content-Type", "application/json")

	err := json.NewEncoder(w).Encode(ErrorResponse{
		StatusCode:  walletError.StatusCode,
		ErrorMessage: walletError.ErrorMessage,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
