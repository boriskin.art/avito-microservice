package usecase

import (
	"avito-microservice/internal/domain"
	"encoding/json"
	"io/ioutil"
	"math"
	"net/http"
)

type WalletUsecase struct {
	walletRepository domain.WalletRepository
}

func NewWalletUsecase(repo domain.WalletRepository) *WalletUsecase {
	return &WalletUsecase{walletRepository: repo}
}

func (wu *WalletUsecase) GetBalance(userID int) (float64, error) {
	return wu.walletRepository.GetBalance(userID)
}

func (wu *WalletUsecase) UpdateBalance(userID int, operationType string, amount float64) *domain.WalletError {
	if isExist, _ := wu.walletRepository.CheckUserExistence(userID); !isExist {
		if err := wu.walletRepository.CreateUserWallet(userID); err != nil {
			return domain.NewWalletError(500, "Creating user error")
		}
	}

	if amount <= 0 {
		return domain.IncorrectAmount
	}

	if operationType == "expenditure" {
		userBalance, err := wu.walletRepository.GetBalance(userID)
		if err != nil {
			return domain.UserDoesNotExistError
		}
		if userBalance < amount {
			return domain.NotEnoughMoney
		}
		amount = -amount
	} else if operationType != "refill" {
		return domain.InvalidOperationType
	}

	err := wu.walletRepository.UpdateBalance(userID, amount)

	if err != nil {
		return domain.NewWalletError(500, "Update balance error")
	}
	return nil
}

func (wu *WalletUsecase) TransferFunds(fromUserID, toUserID int, amount float64) *domain.WalletError {
	if amount <= 0 {
		return domain.IncorrectAmount
	}

	if isExist, _ := wu.walletRepository.CheckUserExistence(toUserID); !isExist {
		if err := wu.walletRepository.CreateUserWallet(toUserID); err != nil {
			return domain.NewWalletError(500, "Transfer funds error")
		}
	}

	userBalance, err := wu.GetBalance(fromUserID)
	if err != nil {
		return domain.UserDoesNotExistError
	}
	if userBalance < amount {
		return domain.NotEnoughMoney
	}

	e := wu.walletRepository.TransferFunds(fromUserID, toUserID, amount)
	if e != nil {
		return e.(*domain.WalletError)
	}
	return nil

}

func (wu *WalletUsecase) ConvertCurrency(currency string, amount float64) (float64, *domain.WalletError) {
	price := 1.0
	if currency != "" {
		var err error
		price, err = wu.GetCurrencyData(currency)
		if err != nil {
			return 0, domain.ParseCurrencyDataError
		}
	}

	return math.Round(amount * price * 100) / 100, nil
}

func (wu *WalletUsecase) GetCurrencyData(currency string) (float64, *domain.WalletError) {
	// Get data from API
	response, err := http.Get("https://www.cbr-xml-daily.ru/latest.js")
	if err != nil {
		return 0, domain.GetCurrencyDataError
	}

	// Read data to []byte
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return 0, domain.ParseCurrencyDataError

	}

	// Parse data to interface
	var currencyData interface{}
	err = json.Unmarshal(body, &currencyData)
	if err != nil {
		return 0, domain.ParseCurrencyDataError

	}

	// Convert interface to maps and get currency rate
	rates := currencyData.(map[string]interface{})["rates"].(map[string]interface{})
	currencyRate, ok := rates[currency]
	if !ok {
		return 0, domain.ParseCurrencyDataError
	}

	return currencyRate.(float64), nil
}