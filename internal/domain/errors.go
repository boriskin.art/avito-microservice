package domain

type WalletError struct {
	StatusCode   int
	ErrorMessage string
}

func NewWalletError(statusCode int, errorMessage string) *WalletError {
	return &WalletError{
		StatusCode: statusCode,
		ErrorMessage: errorMessage,
	}
}

func (we WalletError) Error() string {
	return we.ErrorMessage
}

var (
	UserDoesNotExistError  = NewWalletError(100, "User doesn't exist")
	NotEnoughMoney 		   = NewWalletError(200, "Not enough money")
	IncorrectAmount        = NewWalletError(201, "Incorrect amount")
	InvalidOperationType   = NewWalletError(300, "Invalid operation type")
	URLParamsError         = NewWalletError(301, "Error while parsing url parameters")
	InputDataError         = NewWalletError(302, "Input data error")
	TransferringError      = NewWalletError(400, "Error while transferring funds")

	GetCurrencyDataError   = NewWalletError(600, "Get currency API error")
	ParseCurrencyDataError = NewWalletError(601, "Parse currency data error")

)