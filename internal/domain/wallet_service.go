package domain

type Wallet struct {
	UserID  int
	Balance float64
}

type WalletRepository interface {
	GetBalance(userID int) (float64, error)
	CheckUserExistence(userID int) (bool, error)
	CreateUserWallet(userID int) error
	UpdateBalance(userID int, amount float64) error
	TransferFunds(fromUserID, toUserID int, amount float64) error
}

type WalletUsecase interface {
	GetBalance(userID int) (float64, error)
	UpdateBalance(userID int, operationType string, amount float64) *WalletError
	TransferFunds(fromUserID, toUserID int, amount float64) *WalletError
	GetCurrencyData(currency string) (float64, *WalletError)
	ConvertCurrency(currency string, amount float64) (float64, *WalletError)
}