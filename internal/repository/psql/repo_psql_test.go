package psql

import (
	"avito-microservice/internal/domain"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestWalletPsqlRepository_GetBalance(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := NewWalletPsqlRepository(db)

	wallet := domain.Wallet{
		UserID:  1,
		Balance: 100.0,
	}

	rows := sqlmock.NewRows([]string{"balance"}).
		AddRow(wallet.Balance)


	// good query
	mock.ExpectQuery("SELECT balance FROM wallet").
		WithArgs(wallet.UserID).
		WillReturnRows(rows)

	result, err := repo.GetBalance(wallet.UserID)

	assert.NoError(t, err)
	assert.Equal(t, wallet.Balance, result)
	assert.NoError(t, mock.ExpectationsWereMet())

	// bad query
	mock.ExpectQuery("SELECT balance FROM wallet").
		WithArgs(wallet.UserID).
		WillReturnError(fmt.Errorf("db_error"))

	result, err = repo.GetBalance(wallet.UserID)

	assert.Error(t, err)
	assert.Equal(t, float64(0), result)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestWalletPsqlRepository_UpdateBalance(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := NewWalletPsqlRepository(db)

	var userID = 1
	var amount = 100.5

	// good query
	mock.ExpectExec("UPDATE").
		WithArgs(amount, userID).
		WillReturnResult(
			sqlmock.NewResult(1, 1))

	err = repo.UpdateBalance(userID, amount)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())

	// bad query
	mock.ExpectExec("UPDATE").
		WithArgs(amount, userID).
		WillReturnError(fmt.Errorf("db_error"))

	err = repo.UpdateBalance(userID, amount)
	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestWalletPsqlRepository_CreateUserWallet(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := NewWalletPsqlRepository(db)

	wallet := domain.Wallet{
		UserID:  1,
	}

	// good query
	mock.ExpectExec("INSERT").
		WithArgs(wallet.UserID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.CreateUserWallet(wallet.UserID)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())

	// bad query
	mock.ExpectExec("INSERT").
		WithArgs(wallet.UserID).
		WillReturnError(fmt.Errorf("db_error"))

	err = repo.CreateUserWallet(wallet.UserID)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestWalletPsqlRepository_CheckUserExistence(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := NewWalletPsqlRepository(db)

	wallet := domain.Wallet{
		UserID:  1,
	}


	// user exist
	rows := sqlmock.NewRows([]string{"result"}).AddRow(true)

	mock.ExpectQuery("select exists").
		WithArgs(wallet.UserID).
		WillReturnRows(rows)

	result, err := repo.CheckUserExistence(wallet.UserID)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.True(t, result)


	// user doesn't exist
	rows = sqlmock.NewRows([]string{"result"}).AddRow(false)

	mock.ExpectQuery("select exists").
		WithArgs(wallet.UserID).
		WillReturnRows(rows)

	result, err = repo.CheckUserExistence(wallet.UserID)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.False(t, result)

	// bad query
	mock.ExpectQuery("select exists").
		WithArgs(wallet.UserID).
		WillReturnError(fmt.Errorf("db_error"))

	result, err = repo.CheckUserExistence(wallet.UserID)
	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.False(t, result)
}

func TestWalletPsqlRepository_TransferFunds(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := NewWalletPsqlRepository(db)

	var amount = 55.5

	wallet1 := domain.Wallet{
		UserID:  1,
		Balance: 100,
	}

	wallet2 := domain.Wallet{
		UserID:  2,
		Balance: 30,
	}

	// good query
	mock.ExpectBegin()
	mock.ExpectExec("UPDATE wallet SET balance = balance - ").
		WithArgs(amount, wallet1.UserID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectExec("UPDATE wallet SET balance = balance + ").
		WithArgs(amount, wallet2.UserID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectCommit()

	err = repo.TransferFunds(wallet1.UserID, wallet2.UserID, amount)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())

	// bad query on 1 exec
	mock.ExpectBegin()
	mock.ExpectExec("UPDATE wallet SET balance = balance - ").
		WithArgs(amount, wallet1.UserID).
		WillReturnError(fmt.Errorf("db_error"))
	mock.ExpectRollback()

	err = repo.TransferFunds(wallet1.UserID, wallet2.UserID, amount)
	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())

	// bad query on 2 exec
	mock.ExpectBegin()
	mock.ExpectExec("UPDATE wallet SET balance = balance - ").
		WithArgs(amount, wallet1.UserID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("UPDATE wallet SET balance = balance + ").
		WithArgs(amount, wallet2.UserID).
		WillReturnError(fmt.Errorf("db_error"))
	mock.ExpectRollback()

	err = repo.TransferFunds(wallet1.UserID, wallet2.UserID, amount)
	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}