package psql

import (
	"database/sql"
)

type WalletPsqlRepository struct {
	DB *sql.DB
}

func NewWalletPsqlRepository(db *sql.DB) *WalletPsqlRepository {
	return &WalletPsqlRepository{DB: db}
}


func (wr *WalletPsqlRepository) GetBalance(userID int) (float64, error) {
	var userBalance float64

	err := wr.DB.QueryRow(
		"SELECT balance FROM wallet WHERE user_id=$1",
		userID).
		Scan(&userBalance)

	if err != nil {
		return 0, err
	}

	return userBalance, nil
}

func (wr *WalletPsqlRepository) CheckUserExistence(userID int) (bool, error) {
	var result bool
	err := wr.DB.QueryRow("select exists(select 1 from wallet where user_id=$1)", userID).Scan(&result)
	return result, err
}

func (wr *WalletPsqlRepository) CreateUserWallet(userID int) error {
	_, err := wr.DB.Exec("INSERT INTO wallet (user_id, balance) VALUES ($1, 0)", userID)
	if err != nil {
		return err
	}

	return nil
}

func (wr *WalletPsqlRepository) UpdateBalance(userID int, amount float64) error {
	_, err := wr.DB.Exec(
		"UPDATE wallet SET balance = balance + $1 WHERE user_id=$2", amount, userID)
	if err != nil {
		return err
	}

	return nil
}

func (wr *WalletPsqlRepository) TransferFunds(fromUserID, toUserID int, amount float64) error {
	// Start transaction
	tx, err := wr.DB.Begin()
	if err != nil {
		return err
	}

	_, err = tx.Exec("UPDATE wallet SET balance = balance - $1 WHERE user_id=$2", amount, fromUserID)
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = tx.Exec(
		  "UPDATE wallet SET balance = balance + $1 WHERE user_id=$2",
		amount, toUserID)
	if err != nil {
		tx.Rollback()
		return err
	}

	// End of transaction
	err = tx.Commit()
	return err
}